/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{useState,useEffect} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import OneSignal from 'react-native-onesignal';



const App = () =>{
  const isDarkMode = useColorScheme() === 'dark';
  const [isSubscribed,setIsSubscribed] = useState<boolean>(false);


  useEffect(async() => {
    
      /* O N E S I G N A L   S E T U P */

      
        console.log('=====> here');
        OneSignal.setLogLevel(6, 0);
        OneSignal.setAppId("cf6bc55e-d5f2-453a-adbc-823bb02a198e");
        
        // OneSignal.promptForPushNotificationsWithUserResponse(response => {
        //   console.log("Prompt response:", response);
        // });

        OneSignal.setNotificationWillShowInForegroundHandler(notificationReceivedEvent => {
          console.log("OneSignal: notification will show in foreground:", notificationReceivedEvent);
          let notification = notificationReceivedEvent.getNotification();
          console.log("notification: ", notification);
          const data = notification.additionalData
          console.log("additionalData: ", data);
        });
        
        OneSignal.setNotificationOpenedHandler(notification => {
          console.log("OneSignal: notification opened:", notification);
        });
        const deviceState = await OneSignal.getDeviceState();
        
        setIsSubscribed(deviceState.isSubscribed);

        
  },[]);

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  //console.log(isSubscribed);

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <Header />
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}>

          <LearnMoreLinks />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};



export default App;




